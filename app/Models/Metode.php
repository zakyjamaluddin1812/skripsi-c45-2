<?php

namespace App\Models;

use CodeIgniter\Model;

class Metode extends Model
{
    protected $ponsel;
    protected $attribute;
    protected $node1;
    protected $node2;
    
    public function __construct () {
        $this->ponsel = model('Ponsel');
        $this->attribute = [
            ['kategori' => [
                'Oppo',
                'Vivo'
            ]],
            ['harga' => [
                'MURAH',
                'MAHAL'
            ]]
        ];
    }
    public function pulihkan() {
        $this->attribute = [
            ['kategori' => [
                'Oppo',
                'Vivo'
            ]],
            ['harga' => [
                'MURAH',
                'MAHAL'
            ]]
        ];
    }

    public function node1() {
        $this->pulihkan();

        $jumlah = count($this->ponsel->findAll());
        $jumlahYa = count($this->ponsel->where('terjual', 'LAKU')->findAll());
        $jumlahTidak = count($this->ponsel->where('terjual', 'KURANG LAKU')->findAll());

        $entrophy = $this->getEntrophy($jumlah, $jumlahYa, $jumlahTidak);

        $attr = $this->getAttr($this->attribute, $jumlah, $entrophy);
        $node = $this->getAttrNode1($attr);
        // dd($attr);


        
        return $node;

    }

    public function getAttrNode1($attribute) {
        $this->pulihkan();
        $nama = array_keys($attribute);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        $this->node1 = $index;
        
        unset($this->attribute[$this->node1]);
        $key = array_keys($attribute)[0];
        $values = array_values($attribute)[0];
        $wadah = [];
        foreach ($values as $value) {
            $jumlahBarang = count($this->ponsel->where($key, $value)->findAll());
            $jumlahYa = count($this->ponsel->where($key, $value)->where('terjual', 'LAKU')->findAll());
            $jumlahTidak = count($this->ponsel->where($key, $value)->where('terjual', 'TIDAK LAKU')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            $attr = $this->getAttr($this->attribute, $jumlahBarang, $entrophy);
            $nama = array_keys($attr)[0];

            $decissions = [];
            foreach ($attr[$nama] as $val) {
                $jumlahBarang = count($this->ponsel->where($key, $value)->where($nama, $val)->findAll());
                $jumlahYa = count($this->ponsel->where($key, $value)->where($nama, $val)->where('terjual', 'LAKU')->findAll());
                $jumlahTidak = count($this->ponsel->where($key, $value)->where($nama, $val)->where('terjual', 'KURANG LAKU')->findAll());

                $data = [
                    "key" => $nama,
                    "value" => $val,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak
                ];
                array_push($decissions, $data);
            }



            // dd($attr);
            // $node = $this->getAttrNode2($attr, $key, $value);

            $data = [
                "nama" => $key,
                "value" => $value,
                "data" => $decissions
            ];
            array_push($wadah, $data);
            // dd($wadah);

        }

        return $wadah;
        
    }

    

    public function getAttr($attribute, $jumlahTotal, $entrophyTotal) {
        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->ponsel->where($attrName, $value)->findAll());
                $jumlahYa = count($this->ponsel->where($attrName, $value)->where('terjual', 'LAKU')->findAll());
                $jumlahTidak = count($this->ponsel->where($attrName, $value)->where('terjual', 'KURANG LAKU')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }
            array_push($wadah_data_smt, $wadahEntrophy);
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }

            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }

        // dd($wadahGain);
        $keys = [];
        $values = [];
        foreach ($wadahGain as $gain) {
            $key = $gain['nama'];
            $value = $gain['gain'];
            array_push($keys, $key);
            array_push($values, $value);
        }
        $gainMax = max($values);
        $index = array_search($gainMax, $values);
        $key = $keys[$index];
        $labels = [];
        $ponsel = $this->ponsel->select($key)->findAll();
        foreach($ponsel as $b) {
            array_push($labels, $b[$key]);
        }
        $label = array_unique($labels);
        $data = [
            $key => $label
        ];
        return $data;
    }


    public function getEntrophy ($jumlahBarang, $jumlahYa, $jumlahTidak) {
        if($jumlahYa == 0 || $jumlahTidak == 0) {
            return 0;
        }
        $entrophy = (-$jumlahTidak/$jumlahBarang * log($jumlahTidak/$jumlahBarang, 2)) + (-$jumlahYa/$jumlahBarang * log($jumlahYa/$jumlahBarang, 2));
        return round($entrophy, 3);
    }

    public function getGain ($entrophyTotal, $jumlahTotal, $entrophyAttr1, $jumlahAttr1, $entrophyAttr2, $jumlahAttr2) {       
        if($entrophyTotal == 0 || $jumlahTotal == 0 || $entrophyAttr1 == 0 || $jumlahAttr1 == 0 || $entrophyAttr2 == 0 || $jumlahAttr2 == 0) {
            return 0;
        }
        
        $attr1 = round($jumlahAttr1/$jumlahTotal*$entrophyAttr1, 3);
        $attr2 = round($jumlahAttr2/$jumlahTotal*$entrophyAttr2, 3);
        $gain = round($entrophyTotal-($attr1+$attr2), 3);
        return round($gain, 3);

    }
    public function getGains ($entrophyTotal, $jumlahTotal, $entrophyAttr1, $jumlahAttr1, $entrophyAttr2, $jumlahAttr2, $entrophyAttr3, $jumlahAttr3) {
        if($entrophyTotal == 0 || $jumlahTotal == 0 || $entrophyAttr1 == 0 || $jumlahAttr1 == 0 || $entrophyAttr2 == 0 || $jumlahAttr2 == 0 || $entrophyAttr3 == 0 || $jumlahAttr3 == 0) {
            return 0;
        }
        $attr1 = round($jumlahAttr1/$jumlahTotal*$entrophyAttr1, 3);
        $attr2 = round($jumlahAttr2/$jumlahTotal*$entrophyAttr2, 3);
        $attr3 = round($jumlahAttr3/$jumlahTotal*$entrophyAttr3, 3);
        $gain = round($entrophyTotal-($attr1+$attr2+$attr3), 3);
        return $gain;

    }











    public function total() {
        $this->pulihkan();

        $jumlahBarang = count($this->ponsel->findAll());
        $jumlahYa = count($this->ponsel->where('terjual', 'LAKU')->findAll());
        $jumlahTidak = count($this->ponsel->where('terjual', 'KURANG LAKU')->findAll());
        $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
        $total = [
            "total" => $jumlahBarang,
            "ya" => $jumlahYa,
            "tidak" => $jumlahTidak,
            "entrophy" => $entrophy
        ];
        return $total;

    }


    public function total_attr () {
        $this->pulihkan();

        $jumlahTotal = count($this->ponsel->findAll());
        $jumlahYa = count($this->ponsel->where('terjual', 'LAKU')->findAll());
        $jumlahTidak = count($this->ponsel->where('terjual', 'KURANG LAKU')->findAll());
        $entrophyTotal = $this->getEntrophy($jumlahTotal, $jumlahYa, $jumlahTidak);

        $wadahGain = [];
        foreach ($this->attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->ponsel->where($attrName, $value)->findAll());
                $jumlahYa = count($this->ponsel->where($attrName, $value)->where('terjual', 'LAKU')->findAll());
                $jumlahTidak = count($this->ponsel->where($attrName, $value)->where('terjual', 'KURANG LAKU')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);

                $namas = [];
                foreach ($this->attribute as $q) {
                    $nama = array_keys($q)[0];
                    array_push($namas, $nama);
                }
                $this->node1 = array_search($attrName, $namas);
            }

            
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }

        return $wadahGain;


        // test
    }
    public function node1_attr () {
        $this->pulihkan();

        $jumlahTotal = count($this->ponsel->findAll());
        $jumlahYa = count($this->ponsel->where('terjual', 'LAKU')->findAll());
        $jumlahTidak = count($this->ponsel->where('terjual', 'KURANG LAKU')->findAll());
        $entrophyTotal = $this->getEntrophy($jumlahTotal, $jumlahYa, $jumlahTidak);

        $wadahGain = [];
        foreach ($this->attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            $wadahEntrophy2 = [];

            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->ponsel->where($attrName, $value)->findAll());
                $jumlahYa = count($this->ponsel->where($attrName, $value)->where('terjual', 'LAKU')->findAll());
                $jumlahTidak = count($this->ponsel->where($attrName, $value)->where('terjual', 'KURANG LAKU')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);

                $namas = [];
                foreach ($this->attribute as $q) {
                    $nama = array_keys($q)[0];
                    array_push($namas, $nama);
                }
                $index_nama = array_search($attrName, $namas);
                
                // dd($this->attribute);
                unset($this->attribute[$this->node1]);
                $entahlah = [];
                foreach ($this->attribute as $attr) {
                    $attrName1 = array_keys($attr)[0];
                    $wadahEntrophy = [];
                    foreach ($attr[$attrName1] as $value1) {
                        $att = $attrName;
                        $vae = $value;
                        // dd($att, $vae, $attrName1, $value1);
                        $satu = $this->ponsel->where($attrName1, $value1)->where($att, $vae)->findAll();
                        $dua = $this->ponsel->where($attrName1, $value1)->where($att, $vae)->where('terjual', 'LAKU')->findAll();
                        $tiga = $this->ponsel->where($attrName1, $value1)->where($att, $vae)->where('terjual', 'KURANG LAKU')->findAll();

                        // dd($satu, $dua);





                        $jumlah = count($satu);
                        $ya = count($dua);
                        $tidak = count($tiga);
                        $entrophy = $this->getEntrophy($jumlah, $ya, $tidak);
                        // dd($jumlah, $ya, $tidak, $entrophy);
                        $data1 = [
                            "key" => $attrName1,
                            "value" => $value1,
                            "jumlah" => $jumlah,
                            "jumlahYa" => $ya,
                            "jumlahTidak" => $tidak,
                            "entrophy" => $entrophy
                        ];
                        array_push($entahlah, $data1);

                        
                    }
                    array_push($wadahEntrophy2, $entahlah);
                    
                }
                
                
                
                
                
            }
            
        }
        return $wadahEntrophy2;
        // dd($wadahEntrophy2, $attrName, $attrName1);

        // return $wadahEntrophy2;


        // test
    }
}