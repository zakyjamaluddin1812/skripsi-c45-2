<?php

namespace App\Models;

use CodeIgniter\Model;

class Ponsel extends Model
{
    protected $table = "ponsel";
    protected $allowedFields = ['type_handphone', 'nominal', 'harga', 'terjual', 'kategori'];
}