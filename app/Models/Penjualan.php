<?php

namespace App\Models;

use CodeIgniter\Model;

class Penjualan extends Model
{
    protected $table = "penjualan";
    protected $allowedFields = ['id_ponsel', 'bulan', 'jumlah'];
}