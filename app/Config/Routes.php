<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
//$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');

$routes->get('/dashboard', 'Home::index');
$routes->get('/detail/(:num)', 'Home::detail/$1');
$routes->get('/add', 'Home::add');
$routes->get('/ubah/(:num)', 'Home::ubah/$1');
$routes->post('/simpan', 'Home::simpan');
$routes->post('/update', 'Home::update');
$routes->get('/hapus/(:num)', 'Home::hapus/$1');

$routes->get('penjualan', 'Penjualan::index');
$routes->get('detail_penjualan/(:num)', 'Penjualan::detail/$1');
$routes->post('tambah_penjualan/(:num)', 'Penjualan::tambah/$1');
$routes->post('ubah_penjualan/(:num)', 'Penjualan::ubah/$1');
$routes->get('hapus_penjualan/(:num)', 'Penjualan::hapus/$1');

$routes->get('akun', 'Akun::index');
$routes->get('detail-akun/(:num)', 'Akun::detail/$1');
$routes->get('hapus-akun/(:num)', 'Akun::hapus/$1');
$routes->get('ubah-admin/(:num)', 'Akun::ubah_admin/$1');
$routes->get('ubah-staff/(:num)', 'Akun::ubah_staff/$1');


$routes->get('/prediksi', 'Prediksi::index');
$routes->get('/', function() {
    if(session()->get('login') == true) {
        return redirect()->to(base_url('/dashboard'));
    }
    return view('header_auth')
    .view('login')
    .view('home')
    .view('footer');
});
$routes->get('/daftar', function() {
    if(session()->get('login') == true) {
        return redirect()->to(base_url('/dashboard'));
    }
    return view('header_auth')
    .view('daftar')
    .view('home')
    .view('footer');
});
$routes->post('/daftar', 'Auth::daftar');
$routes->post('/login', 'Auth::login');
$routes->get('/prediksi-detail', 'Prediksi::detail');
$routes->get('/prediksi-cek', 'Prediksi::cek');
$routes->post('/prediksi-cek', 'Prediksi::show');
$routes->get('/logout', 'Auth::logout');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
