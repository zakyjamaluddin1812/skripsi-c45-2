<?php

namespace App\Controllers;

class Penjualan extends BaseController
{
    protected $ponsel;
    protected $penjualan;
    protected $session;
    protected $users;

    public function __construct() {
        $this->ponsel = model('../Models/Ponsel');
        $this->penjualan = model('../Models/Penjualan');
        $this->users = model('../Models/User');
        $this->session = session();
    }

    public function index() {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['user'] = $this->users->find($this->session->get('user'));
        $data['ponsel'] = $this->ponsel->findAll();
        return view('header', $data)
        .view('penjualan', $data)
        .view('home')   
        .view('footer');
    }
    public function detail($id) {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['user'] = $this->users->find($this->session->get('user'));

        $data['ponsel_detail'] = $this->ponsel->find($id);
        $data['penjualan'] = $this->penjualan->where('id_ponsel', $id)->findAll();
        return view('header', $data)
        .view('penjualan', $data)
        .view('penjualan_detail', $data)   
        .view('footer');
    }
    public function tambah($id) {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['user'] = $this->users->find($this->session->get('user'));

        $data['ponsel_detail'] = $this->ponsel->find($id);
        $data['penjualan'] = $this->penjualan->where('id_ponsel', $id)->findAll();

        $penjualan = [
            "id_ponsel" => $id,
            "bulan" => $this->request->getPost('bulan'),
            "jumlah" => $this->request->getPost('jumlah')
        ];

        $this->penjualan->insert($penjualan);


        return redirect()->back();
    }

    public function ubah($id) {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['ponsel_detail'] = $this->ponsel->find($id);

        $data['penjualan'] = $this->penjualan->where('id_ponsel', $id)->findAll();

        $penjualan = [
            "bulan" => $this->request->getPost('bulan'),
            "jumlah" => $this->request->getPost('jumlah')
        ];
        // dd($this->penjualan->find($id));
        $this->penjualan->update($id, $penjualan);

        // $this->penjualan->update($penjualan, ['id' => $id]);


        return redirect()->back();
    }
    public function hapus($id) {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['ponsel_detail'] = $this->ponsel->find($id);

        $data['penjualan'] = $this->penjualan->where('id_ponsel', $id)->findAll();

       
        $this->penjualan->delete($id);

        return redirect()->back();
    }
}
