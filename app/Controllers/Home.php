<?php

namespace App\Controllers;

class Home extends BaseController
{
    protected $ponsel;
    protected $session;
    protected $users;
    public function __construct() {
        $this->ponsel = model('../Models/Ponsel');
        $this->users = model('../Models/User');
        $this->session = session();
    }
    public function index()
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['user'] = $this->users->find($this->session->get('user'));

        return view('header', $data)
        .view('dashboard', $data)
        .view('home')
        .view('footer');
    }

    public function detail($id)
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['detail'] = $this->ponsel->find($id);
        $data['user'] = $this->users->find($this->session->get('user'));


        return view('header', $data)
        .view('dashboard', $data)
        .view('detail', $data)
        .view('footer');
    }

    public function add() 
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['user'] = $this->users->find($this->session->get('user'));

        return view('header', $data)
        .view('dashboard', $data)
        .view('tambah')
        .view('footer');
    }
    public function ubah($id) 
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['ponsel'] = $this->ponsel->findAll();
        $data['detail'] = $this->ponsel->find($id);
        $data['user'] = $this->users->find($this->session->get('user'));


        return view('header', $data)
        .view('dashboard', $data) 
        .view('ubah', $data)
        .view('footer');
    }

    public function simpan() 
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $type_handphone = $this->request->getPost('type_handphone');
        $nominal = (int)$this->request->getPost('nominal');
        $merek = $this->request->getPost('merek');
        $harga = '';
        if($nominal < 1500000) {
            $harga = "MURAH";
        } else {
            $harga = "MAHAL";
        }


        $terjual = '';

        if($harga == 'MURAH') {
            if($merek == 'Oppo') {
                $terjual = 'LAKU';
            } else {
                $terjual = 'KURANG LAKU';
            }

        } else {
            if($merek == 'Oppo') {
                $terjual = 'KURANG LAKU';
            } else {
                $terjual = 'LAKU';
            }
        }

        $ponsel = [
            "type_handphone" => $type_handphone,
            "nominal" => $nominal,
            "harga" => $harga,
            "terjual" => $terjual,
            "kategori" => $merek
        ];
        $this->ponsel->insert($ponsel);
        return redirect()->to(base_url('/dashboard'));
    }
    public function update() 
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $id = $this->request->getPost('id');
        $type_handphone = $this->request->getPost('type_handphone');
        $nominal = (int)$this->request->getPost('nominal');
        $merek = $this->request->getPost('merek');
        $harga = '';
        if($nominal < 1500000) {
            $harga = "MURAH";
        } else {
            $harga = "MAHAL";
        }

        $ponsel = [
            "type_handphone" => $type_handphone,
            "harga" => $harga,
            "merek" => $merek
        ];

        $this->ponsel->update($id, $ponsel);
        return redirect()->to(base_url('/dashboard'));
    }

    public function hapus($id) {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $this->ponsel->delete($id);
        return redirect()->to(base_url('/dashboard'));
    }
}

