<?php

namespace App\Controllers;

class Auth extends BaseController
{
    protected $session;

    protected $user;
    public function __construct() {
        $this->session = session();
        $this->user = model('../Models/User');
    }
    public function daftar() {
        if($this->session->get('login') == true) {
            return redirect()->to(base_url('/dashboard'));
        }
        $nama = $this->request->getPost('nama');
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $password_confirm = $this->request->getPost('password_confirm');

        if($password != $password_confirm) {
            return redirect()->back()->with('message', 'salah');
        }

        $user = [
            "nama" => $nama,
            "email" => $email,
            "password" => $password,
            "role" => "staff"
        ];

        $this->user->insert($user);
        $this->session->setFlashData('message', 'daftar');
        return redirect()->to(base_url('/'));



    }
    public function login() {
        if($this->session->get('login') == true) {
            return redirect()->to(base_url('/dashboard'));
        }
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $user = $this->user->where('email', $email)->where('password', $password)->findAll();
        if($user == null) {
            return redirect()->back()->with('message', 'gagal');
        }

        $this->session->set('login', true);
        $this->session->set('user', $user[0]['id']);
        return redirect()->to(base_url('/dashboard'));

    }

    public function logout() {
        $this->session->remove('login');
        return redirect()->to(base_url('/'));
    }
}