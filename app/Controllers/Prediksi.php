<?php

namespace App\Controllers;

class Prediksi extends BaseController
{
    protected $ponsel;
    protected $metode;
    protected $session;
    protected $users;
    public function __construct() {
        $this->ponsel = model('../Models/Ponsel');
        $this->metode = model('../Models/Metode');
        $this->users = model('../Models/User');
        $this->session = session();
    }
    public function index()
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['node'] = $this->metode->node1();
        $data['user'] = $this->users->find($this->session->get('user'));

        // dd($data['total'], $data['total_attr']);

        return view('header', $data)
        .view('decission_tree', $data)
        .view('home')
        .view('footer');
    }
    public function detail()
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['node'] = $this->metode->node1();
        $data['total'] = $this->metode->total();
        $data['total_attr'] = $this->metode->total_attr();
        $data['user'] = $this->users->find($this->session->get('user'));


        $data['node1_attr'] = $this->metode->node1_attr();

        // dd($data['total'], $data['total_attr'], $data['node1_attr']);

        return view('header', $data)
        .view('decission_tree', $data)
        .view('decission_detail', $data)
        .view('footer');
    }


    public function cek()
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['node'] = $this->metode->node1();

        $data['user'] = $this->users->find($this->session->get('user'));

        return view('header', $data)
        .view('decission_tree', $data)
        .view('decission_cek')
        .view('footer');
    }

    public function show()
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $nominal = $this->request->getPost('nominal');
        $merek = $this->request->getPost('merek');
        $harga = '';
        if($nominal < 1500000) {
            $harga = "MURAH";
        } elseif($nominal >= 1500000) {
            $harga = "MAHAL";
        } else {
            $this->session->setFlashData('terjual', "Error");
        // $data['node'] = $this->metode->node1();

        return redirect()->back();
        }

        $terjual = '';

        if($harga == 'MURAH') {
            if($merek == 'Oppo') {
                $terjual = 'LAKU';
            } else {
                $terjual = 'KURANG LAKU';
            }

        } else {
            if($merek == 'Oppo') {
                $terjual = 'KURANG LAKU';
            } else {
                $terjual = 'LAKU';
            }
        }

        // dd($harga, $merek, $terjual);

        // $data['decission'] = $terjual;
        $this->session->setFlashData('terjual', $terjual);
        // $data['node'] = $this->metode->node1();

        return redirect()->back();
    }
}