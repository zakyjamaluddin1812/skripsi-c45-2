<?php

namespace App\Controllers;

class Akun extends BaseController
{
    protected $ponsel;
    protected $users;
    protected $session;
    public function __construct() {
        $this->ponsel = model('../Models/Ponsel');
        $this->users = model('../Models/User');
        $this->session = session();
    }
    public function index()
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['users'] = $this->users->findAll();
        $data['user'] = $this->users->find($this->session->get('user'));

        return view('header', $data)
        .view('akun', $data)
        .view('home')
        .view('footer');
    }
    public function detail($id)
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $data['users'] = $this->users->findAll();
        $data['detail'] = $this->users->find($id);
        $data['user'] = $this->users->find($this->session->get('user'));

        return view('header', $data)
        .view('akun', $data)
        .view('detail_akun')
        .view('footer');
    }
    public function hapus($id)
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $this->users->delete($id);
        $this->session->setFlashData('message', 'Akun terhapus');
        return redirect()->to(base_url('/akun'));
    }
    public function ubah_admin($id)
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $this->users->update($id, ["role" => "admin"]);
        $this->session->setFlashData('message', 'Akun berubah menjadi admin');
        return redirect()->to(base_url('/akun'));
    }
    public function ubah_staff($id)
    {
        if(session()->get('login') != true) {
            return redirect()->to(base_url('/'));
        }
        $this->users->update($id, ["role" => "staff"]);
        $this->session->setFlashData('message', 'Akun berubah menjadi staff');
        return redirect()->to(base_url('/akun'));
    }
}