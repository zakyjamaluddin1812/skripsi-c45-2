<div class="col">
  <h1 class="display-5">Daftar Produk</h1>
  <table class="table table-hover scroll">
    <thead>
      <tr>
        <th scope="col" class="nomor">#</th>
        <th scope="col">Tipe Ponsel</th>
        <th scope="col" class="merek">Merek</th>
      </tr>
    </thead>
    <tbody>
                <?php $x = 1;?>
                <?php foreach($ponsel as $p):?>
              <tr>
                <th scope="row" class="nomor"><?=$x;?></th>
                <td>
                    <a href="/detail/<?=$p['id']?>" class="text-decoration-none text-dark">
                        <?= $p['type_handphone']?>
                    </a>
                </td>
                <td  class="merek"><?= $p['kategori']?></td>
              </tr>
              <?php $x++;?>
              <?php endforeach?>
            </tbody>
          </table>

        <a href="/add"><button class="btn mt-3 btn-success">Tambah Ponsel</button></a>
    </div>