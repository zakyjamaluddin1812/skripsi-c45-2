    
            <div class="col">
                <h1 class="display-5">Detail Akun</h1>
                    <table class="table">
                        <tbody>
                          <tr>
                            <td>Nama</td>
                            <td><?=$detail['nama']?></td>
                          </tr>
                          <tr>
                            <td>Email</td>
                            <td><?=$detail['email']?></td>
                          </tr>
                          <tr>
                            <td>Role</td>
                            <td><?=$detail['role']?></td>
                          </tr>
                        </tbody>
                      </table>
                
                      <?php if($detail['role'] == 'admin') : ?>
                        <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#ubahStaff">Ubah Menjadi Staff</button>
                        <?php endif ?>
                      <?php if($detail['role'] == 'staff') : ?>
                        <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#ubahAdmin">Ubah Menjadi Admin</button>
                        <?php endif ?>
                
                
                <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">Hapus</button>
            </div>





<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Apakah anda yakin?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
        <a href="/hapus-akun/<?= $detail['id']?>" type="button" class="btn btn-danger">Hapus</a>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ubahAdmin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Apakah anda akan mengubah menjadi admin?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
        <a href="/ubah-admin/<?= $detail['id']?>" type="button" class="btn btn-warning">Ubah</a>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ubahStaff" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Apakah anda akan mengubah menjadi staff?
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
        <a href="/ubah-staff/<?= $detail['id']?>" type="button" class="btn btn-warning">Ubah</a>
      </div>
    </div>
  </div>
</div>
        
        