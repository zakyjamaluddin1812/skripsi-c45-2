    
            <div class="col">
                <h1 class="display-5">Ubah</h1>
                <form action="/update" method="post">
                <div class="mb-3">
                <label for="" class="form-label">Tipe Handphone</label>

                                <input type="hidden" name="id" value="<?= $detail['id']?>">
                                <input type="text" name="type_handphone"  class="form-control" aria-label="Username" aria-describedby="basic-addon1" value="<?= $detail['type_handphone']?>">
                            </div>
                <div class="mb-3">
                <label for="" class="form-label">Harga</label>

                                <input type="text" name="nominal" class="form-control"  aria-label="Username" aria-describedby="basic-addon1" value="<?= $detail['nominal']?>">
                            </div>
                <div class="mb-3">
                <label for="" class="form-label">Merek</label>
                        <select required class="form-select" name="merek" aria-label="Default select example">
                            <option  value="">Pilih Merek</option>
                            <option <?= $detail['kategori'] == "Oppo" ? "selected" : ""?> value="Oppo">OPPO</option>
                            <option <?= $detail['kategori'] == "Vivo" ? "selected" : ""?> value="Vivo">Vivo</option>
                        </select>
                            </div>


                <button type="submit" class="btn btn-warning">Simpan</button>
                <a href="/detail/<?= $detail['id']?>" class="btn btn-secondary">Batal</a>
                </form>
            </div>
        
        