<div class="col">
  <h1 class="display-5">Perhitungan Detail</h1>
  <div class="scroll">
  <ul>
    <li>
        <h5>Entrophy Total</h5>
        <table class="table">
  <thead>
    <tr>
      <th scope="col" style="width:25%;">Total</th>
      <th scope="col"  style="width:25%;">Ya</th>
      <th scope="col"  style="width:25%;">Tidak</th>
      <th scope="col"  style="width:25%;">Entrophy</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td style="width:25%;"><?= $total['total']?></td>
        <td style="width:25%;"><?= $total['ya']?></td>
        <td style="width:25%;"><?= $total['tidak']?></td>
        <td style="width:25%;"><?= $total['entrophy']?></td>
    </tr>
  </tbody>
</table>

    </li>
    <li>
        <h5>Mencari Node 1</h5>
        <table class="table">
  <thead>
    <tr>
      <th scope="col" style="width:14%;">Key</th>
      <th scope="col"  style="width:14%;">Gain</th>
      <th scope="col" style="width:14%;">Value</th>
      <th scope="col" style="width:14%;">Jumlah</th>
      <th scope="col"  style="width:14%;">Ya</th>
      <th scope="col"  style="width:14%;">Tidak</th>
      <th scope="col"  style="width:14%;">Entrophy</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($total_attr as $attr) :?>
    <tr>
    <td scope="col" style="width:14%;" rowspan="3"><?= $attr['nama']?></td>
    <td scope="col" style="width:14%;" rowspan="3"><?= $attr['gain']?></td>
    </tr>
    <?php foreach ($attr['detail'] as $a) :?>
    <tr>
      <td scope="col" style="width:14%;"><?= $a['value']?></td>
      <td scope="col" style="width:14%;"><?= $a['jumlah']?></td>
      <td scope="col"  style="width:14%;"><?= $a['jumlahYa']?></td>
      <td scope="col"  style="width:14%;"><?= $a['jumlahTidak']?></td>
      <td scope="col"  style="width:14%;"><?= $a['entrophy']?></td>
    </tr>
    <?php endforeach?>
    <?php endforeach?>
  </tbody>
</table>
    </li>
    <?php $x = 1?>
    <?php foreach ($node1_attr as $attr) :?>

    <li>
        <h5>Mencari Keputusan Dari Node 2.<?= $x?></h5>
        <table class="table">
  <thead>
    <tr>
      <th scope="col" style="width:14%;">Key</th>
      <th scope="col" style="width:14%;">Value</th>
      <th scope="col" style="width:14%;">Jumlah</th>
      <th scope="col"  style="width:14%;">Ya</th>
      <th scope="col"  style="width:14%;">Tidak</th>
      <th scope="col"  style="width:14%;">Entrophy</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($attr as $a) :?>
    <tr>
      
      <td scope="col" style="width:14%;"><?= $a['key']?></td>
      <td scope="col" style="width:14%;"><?= $a['value']?></td>
      <td scope="col" style="width:14%;"><?= $a['jumlah']?></td>
      <td scope="col"  style="width:14%;"><?= $a['jumlahYa']?></td>
      <td scope="col"  style="width:14%;"><?= $a['jumlahTidak']?></td>
      <td scope="col"  style="width:14%;"><?= $a['entrophy']?></td>
    </tr>
    <?php endforeach?>
  </tbody>
</table>
    </li>
    <?php $x++?>

    <?php endforeach?>

  </ul>
  </div>
  
  

    </div>