<div class="col">
  <h1 class="display-5">Daftar Akun</h1>
    <?php if(session()->getFlashData('message') != null) :?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    <?= session()->getFlashData('message')?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php endif?>
    <?php if(session()->getFlashData('error') != null) :?>
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
    <?= session()->getFlashData('error')?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php endif?>
  <table class="table table-hover scroll">
    <thead>
      <tr>
        <th scope="col" class="nomor">#</th>
        <th scope="col">Nama</th>
        <th scope="col" class="merek">Email</th>
      </tr>
    </thead>
    <tbody>
                <?php $x = 1;?>
                <?php foreach($users as $u):?>
              <tr>
                <th scope="row" class="nomor"><?=$x;?></th>
                <td>
                    <a href="/detail-akun/<?=$u['id']?>" class="text-decoration-none text-dark">
                        <?= $u['nama']?>
                    </a>
                </td>
                <td  class="merek"><?= $u['email']?></td>
              </tr>
              <?php $x++;?>
              <?php endforeach?>
            </tbody>
          </table>
    </div>