<div class="col">
  <h1 class="display-5">Decission Tree</h1>
  <table class="table">
  <thead>
    <tr>
      <th scope="col" style="width:33%;">Node1</th>
      <th scope="col"  style="width:33%;">Node 2</th>
      <th scope="col"  style="width:33%;"  class="text-center">Decission</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($node as $n) :?>
    <tr>
      <td rowspan="3" style="width:33%;"><?= $n['nama']?> : <?= $n['value']?></td>
      <!-- <td  style="width:33%;"></td>
      <td  style="width:33%;"></td> -->
    </tr>
    <?php foreach($n['data'] as $o) :?>
    <tr>
      <!-- <td  style="width:33%;"></td> -->
      <td  style="width:33%;"><?= $o['key']?> : <?= $o['value']?></td>
      <td  style="width:33%;" class="text-center">
        <button class="btn btn-sm btn-<?= $o['jumlahTidak'] == 0 ? "success" : ($o['jumlahYa'] == 0 ? "danger" : "warning")?>"><?= $o['jumlahTidak'] == 0 ? "Laku" : ($o['jumlahYa'] == 0 ? "Kurang Laku" : "Tidak Valid")?></button>
        </td>
    </tr>
    <?php endforeach?>
    <?php endforeach?>
  </tbody>
</table>

        <?php if($user['role'] == 'admin') :?>
        <a href="/prediksi-detail"><button class="btn mt-3 btn-success">Detail Decission</button></a>
        <?php endif ?>
        <a href="/prediksi-cek"><button class="btn mt-3 btn-warning">Uji Prediksi</button></a>
    </div>