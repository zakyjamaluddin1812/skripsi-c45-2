<div class="col">
  <h1 class="display-5"><?= $ponsel_detail['type_handphone']?></h1>
  <table class="table table-hover scroll">
    <thead>
      <tr>
        <th scope="col" class="nomor">#</th>
        <th scope="col">Bulan</th>
        <th scope="col" class="">Jumlah</th>
        <th scope="col" class="" colspan=2>Aksi</th>
        <th scope="col" class="">Aksi</th>
      </tr>
    </thead>
    <tbody>
        <?php if($penjualan == null): ?>
            <tr>
                <td colspan=3 class="text-center">
                    Data penjualan masih kosong
                </td>
            </tr>
            <?php else : ?>
                <?php $x = 1;?>
                <?php foreach ($penjualan as $penj) : ?>
              <tr>
                <th scope="row" class="nomor"><?=$x;?></th>
                <td>
                    <a href="/detail/" class="text-decoration-none text-dark">
                    <?=$penj['bulan'];?>
                    </a>
                </td>
                <td  class="merek"><?=$penj['jumlah'];?></td>
                <td>
                <span  data-bs-toggle="modal" data-bs-target="#ubah_penjualan_<?=$penj['id']?>" class="badge badge-sm bg-warning"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg></span>
                </td>
                <td>
                  <span data-bs-toggle="modal" data-bs-target="#hapus_penjualan_<?=$penj['id']?>" class="badge badge-sm bg-danger"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
  <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
</svg></span>
                  
                </td>
                
              </tr>
              <?php $x++;?>
              <?php endforeach?>
              <?php endif?>
            </tbody>
          </table>

          <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#tambah_penjualan">Tambah Penjualan</button>

    </div>


<div class="modal fade" id="tambah_penjualan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Penjualan</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="/tambah_penjualan/<?=$ponsel_detail['id']?>" method="post">
      <div class="modal-body">
        <select class="form-select" name="bulan" required aria-label="Default select example">
            <option selected value="">Pilih Bulan</option>
            <option value="Januari">Januari</option>
            <option value="Pebruari">Pebruari</option>
            <option value="Maret">Maret</option>
            <option value="April">April</option>
            <option value="Mei">Mei</option>
            <option value="Juni">Juni</option>
            <option value="Juli">Juli</option>
            <option value="Agustus">Agustus</option>
            <option value="September">September</option>
            <option value="Oktober">Oktober</option>
            <option value="Nopember">Nopember</option>
            <option value="Desember">Desember</option>
        </select>
        <input type="number" name="jumlah" class="form-control mt-3" id="exampleFormControlInput1" placeholder="Jumlah">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button  type="submit" class="btn btn-success">Tambah penjualan</a>
      </div>
      </form>
    </div>
  </div>
</div>
<?php foreach ($penjualan as $p) : ?>
<div class="modal fade" id="ubah_penjualan_<?=$p['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah Penjualan</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="/ubah_penjualan/<?=$ponsel_detail['id']?>" method="post">
      <div class="modal-body">
        <select class="form-select" name="bulan" required aria-label="Default select example">
            <option value="">Pilih Bulan</option>
            <option <?= $p['bulan'] == 'Januari'? "selected" : ""?>  value="Januari">Januari</option>
            <option <?= $p['bulan'] == 'Pebruari'? "selected" : ""?> value="Pebruari">Pebruari</option>
            <option <?= $p['bulan'] == 'Maret'? "selected" : ""?> value="Maret">Maret</option>
            <option <?= $p['bulan'] == 'April'? "selected" : ""?> value="April">April</option>
            <option <?= $p['bulan'] == 'Mei'? "selected" : ""?> value="Mei">Mei</option>
            <option <?= $p['bulan'] == 'Juni'? "selected" : ""?> value="Juni">Juni</option>
            <option <?= $p['bulan'] == 'Juli'? "selected" : ""?> value="Juli">Juli</option>
            <option <?= $p['bulan'] == 'Agustus'? "selected" : ""?> value="Agustus">Agustus</option>
            <option <?= $p['bulan'] == 'September'? "selected" : ""?> value="September">September</option>
            <option <?= $p['bulan'] == 'Oktober'? "selected" : ""?> value="Oktober">Oktober</option>
            <option <?= $p['bulan'] == 'Nopember'? "selected" : ""?> value="Nopember">Nopember</option>
            <option <?= $p['bulan'] == 'Desember'? "selected" : ""?> value="Desember">Desember</option>
        </select>
        <input type="number" value="<?= $p['jumlah']?>" name="jumlah" class="form-control mt-3" id="exampleFormControlInput1" placeholder="Jumlah">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button  type="submit" class="btn btn-success">Ubah penjualan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
<?php foreach ($penjualan as $p) : ?>
<div class="modal fade" id="hapus_penjualan_<?=$p['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah Penjualan</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Apakah anda yakin?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <a href="/hapus_penjualan/<?=$p['id']?>" class="btn btn-success">Hapus penjualan</a>
      </div>
    </div>
  </div>
</div>
<?php endforeach?>